
extends KinematicBody2D

# member variables here, example:
var GRAVITY = Vector2(0, 9.81) * Globals.get("pixels_per_meter")
var velocity = Vector2(0, 0)
var landed = false

func _ready():
	# Called every time the node is added to the scene.
	pass

func _fixed_process(delta):
	velocity += GRAVITY * delta
	move(velocity * delta)
	set_rot(velocity.angle() - PI/2)
	if test_move(velocity * delta):
		velocity = Vector2(0, 0)
		set_fixed_process(false)
		landed = true

func get_distance():
	return get_node("Tip").get_global_pos().x

func reset():
	landed = false
	velocity = Vector2(0, 0)
	set_fixed_process(false)
	set_rot(0)