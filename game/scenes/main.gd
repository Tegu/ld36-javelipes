
extends Node2D

const PIXELS_PER_METER = 64
const METERS_PER_PES = 0.296
const HIGHSCORE_FILE = "user://highscores"
var player_scn = preload("res://scenes/player.tscn")
var javelin_scn = preload("res://scenes/javelin.tscn")
var camera = null
var player = null
var javelin = null
var distance = null

func _ready():
	# Called every time the node is added to the scene.
	Globals.set("pixels_per_meter", PIXELS_PER_METER)
	camera = get_node("Camera2D")
	player = player_scn.instance()
	add_child(player)
	player.connect("javelin_thrown", self, "_on_player_javelin_thrown")
	javelin = javelin_scn.instance()
	player.give(javelin)
	reset()
	get_tree().set_pause(true)
	set_process_input(true)
	set_fixed_process(true)
	find_node("Highscores").read_scores(HIGHSCORE_FILE)
	generate_marks()

func _fixed_process(delta):
	if distance == null and javelin.landed:
		finished_throw()

func _input(event):
	if event.is_action_released("reset"):
		reset()
	if event.is_action_released("quit"):
		get_tree().quit()

func reset():
	var spawn_point = get_node("PlayerSpawn")
	player.set_global_pos(spawn_point.get_global_pos())
	player.reset()
	javelin.get_parent().remove_child(javelin)
	player.give(javelin)
	javelin.reset()
	distance = null
	find_node("ScorePopup").hide()
	find_node("HiscorePopup").hide()
	find_node("HiscoreLabel").hide()
	var timer = get_node("CameraMoveTimer")
	timer.stop()
	if camera:
		camera.get_parent().remove_child(camera)
		player.add_child(camera)

func finished_throw():
	distance = javelin.get_distance()# - get_plank_pos()
	distance = distance / PIXELS_PER_METER  # in meters
#	print("Distance: %d pedes (%.2f m)" % [distance/METERS_PER_PES, distance])
	var label = find_node("ScoreLabel")
#	label.set_text("%d pedes\n(%.1f m)" % [(distance/METERS_PER_PES), distance])
	label.set_text("%d pedes" % (distance/METERS_PER_PES))
	find_node("ScorePopup").show()
	if player.foul:
		find_node("FoulLabel").show()
	else:
		find_node("FoulLabel").hide()
		var highscores = find_node("Highscores")
		if highscores:
			var result = highscores.add_score(distance/METERS_PER_PES)
			if result >= 0:
				highscores.store_scores(HIGHSCORE_FILE)
				find_node("HiscoreLabel").show()
	find_node("HiscorePopup").show()
	get_node("SamplePlayer2D").play("landing")

func get_plank_pos():
	var platform = get_node("Platform")
	if platform:
		var rect = platform.get_item_rect()
		return platform.get_pos().x + rect.pos.x + rect.size.x
	return 0

func generate_marks():
	var mark_scn = preload("res://scenes/mark.tscn")
	var marks_node = get_node("Marks")
	for pedes in range(25, 401, 25):
		var pos_x = pedes * PIXELS_PER_METER * METERS_PER_PES
		print(pedes, ", ", pos_x)
		var mark = mark_scn.instance()
		mark.set_pos(Vector2(pos_x, 0))
		mark.get_node("Distance").set_text(str(pedes))
		marks_node.add_child(mark)


func _on_player_javelin_thrown():
	var timer = get_node("CameraMoveTimer")
	timer.start()

func _on_CameraMoveTimer_timeout():
	if camera:
		camera.get_parent().remove_child(camera)
		javelin.add_child(camera)
