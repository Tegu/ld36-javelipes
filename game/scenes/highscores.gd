
extends Node

var score_list = null
var max_count = 7

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	score_list = []

func read_scores(filename):
	var file = File.new()
	if file.open(filename, file.READ) == 0:
		score_list = file.get_var()
		file.close()
		return true
	file.close()
	return false

func store_scores(filename):
	var file = File.new()
	file.open(filename, file.WRITE)
	file.store_var(score_list)
	file.close()

func add_score(score):
	# find a place for the new score
	if score_list.empty():
		score_list.append(score)
		return 0
	for i in range(score_list.size()):
		if score > score_list[i]:
			score_list.insert(i, score)
			_clear_excessive()
			return i
	return -1

func _clear_excessive():
	if score_list.size() > max_count:
		for i in range(score_list.size()-1, max_count-1, -1):
			score_list.remove(i)