
extends KinematicBody2D

signal javelin_thrown
const ROT_SPEED = PI
const SLOWDOWN = 250
const STEP_SPEED = Vector2(10, 0)
var previous_leg = null
var velocity = Vector2(0, 0)
var throw_state = "todo"
var foul = false

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	previous_leg = "none"
	velocity = Vector2(0, 0)
	set_process_input(true)
	set_fixed_process(true)

func _input(event):
	if event.is_action_pressed("dash_left"):
		if take_step("left"):
			get_node("Sprite").set_frame(1)
			get_node("SamplePlayer2D").play("left")
	elif event.is_action_pressed("dash_right"):
		if take_step("right"):
			get_node("Sprite").set_frame(2)
			get_node("SamplePlayer2D").play("right")
	elif event.is_action_pressed("throw"):
		if throw_state == "todo":
			throw_state = "aiming"
	elif event.is_action_released("throw"):
		if throw_state == "aiming":
			throw_state = "thrown"
			throw_javelin()
			get_node("Sprite").set_frame(3)
			get_node("SamplePlayer2D").play("throw")

func _fixed_process(delta):
	if throw_state == "aiming":
		rotate_javelin(ROT_SPEED * delta)
	if throw_state != "todo":
		var speed = max(0, velocity.length() - SLOWDOWN * delta)
		velocity = velocity.normalized() * speed
	move(velocity * delta)
	if throw_state != "failed" and get_global_pos().x > 0:
		throw_state = "failed"
		foul = true
		get_node("SamplePlayer2D").play("foul")

func rotate_javelin(angle):
	var javelin = get_node("Hand/Javelin")
	if javelin:
		var rotation = min(PI/2, javelin.get_rot() + angle)
		javelin.set_rot(rotation)

func throw_javelin():
	var javelin = get_node("Hand/Javelin")
	if javelin:
		var global_transform = javelin.get_global_transform()
		javelin.get_parent().remove_child(javelin)
		get_parent().add_child(javelin)
		javelin.set_global_transform(global_transform)
		javelin.velocity = throwing_velocity(velocity, javelin.get_rot())
#		print("Javelin vel: %f" % (javelin.velocity.length() / Globals.get("pixels_per_meter")))
#		print("Throw pos: %f" % (get_global_pos().x / Globals.get("pixels_per_meter")))
		javelin.set_fixed_process(true)
		emit_signal("javelin_thrown")
#		var camera = javelin.get_node("Camera2D")
#		if camera:
#			camera.set_zoom(Vector2(1,1)*3)

func throwing_velocity(movement_velocity, angle):
	return movement_velocity.rotated(angle) * 6

func take_step(leg):
	if leg != previous_leg:
		velocity += STEP_SPEED
		previous_leg = leg
		return true
	return false

func give(node2d):
	var hand = get_node("Hand")
	if hand.get_child_count() == 0:
		hand.add_child(node2d)
		node2d.set_pos(Vector2(0, 0))

func reset():
	velocity = Vector2(0, 0)
	throw_state = "todo"
	previous_leg = null
	foul = false
	get_node("Sprite").set_frame(0)
